# ProSlacker
ProSlacker is a simple plugin for automating repetitive web tasks, such as filling out some risk details on an insurance website.

## Install
 - Head to `chrome://extensions/`
 - Enable `Developer Mode`
 - Click `Load Unpacked`
 - Navigate to the directory you cloned to, and select that folder.

## Usage
Click the plugin to open up the popup.

Actions are separated out arbitrarily per page. The page name at the top of a list of items can be whatever you want. You can expand and collapse the list of items for a specific page.

### Buttons
 - **Run** -  Runs the list of items following the page name.
 - **Reset** - Reset the list to factory defaults. Be careful with this!
 - **Show/Hide** - Hides the list of items for a given page.
 - **-** - Delete a page or item row.
 - **=** - Adjust order of a page or item row.

### Items
An item is defined as a single row, made up of three input boxes. These are the CSS selector, the type of action to run, and the corresponding value of the action.

#### Selector
This is the CSS selector for the element you want to target. Normally, this can be a simple ID.

#### Type
This is the type of event you want to run. So far we have:
 - Click - Accurately simulate a click event on a given element.
 - Event - Use to specify an event such as "change" or "blur".
 - Select - Select an option in a select field.
 - Text - Change the value of a text input field. Not limited to text - can be numbers too.
 - Wait - Wait a given number of miliseconds before proceeding to the next item.

#### Value
This is used to specify a value, if applicable, for the given Type:
 - Event - Specify the name of the event.
 - Select - Specify the value of the option in the select field.
 - Text - Specify the text to enter in the input field.
 - Wait - Specify the number of miliseconds to wait.
