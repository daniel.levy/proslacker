var items;
chrome.storage.local.get(['list','currentPage'], function(result) {
    var list = result.list;
    var currentPage = result.currentPage;

    var page = document.querySelector("body").id;

    var obj = list.find(function(el) {
        return el.page == currentPage;
    });

    items = obj.items;

    if(items != null) {
        doLoop(0);
    }
});



function doLoop(i) {

    if(i >= items.length) {
        return;
    }
    var el = items[i];
    i++;
    try {
        switch(el.type) {
            case "text" :
                document.querySelector(el.selector).value = el.value;
                doLoop(i);
                break;
            case "select" :
                document.querySelector(el.selector + " option[value='"+el.value+"']").setAttribute("selected","selected");
				document.querySelector(el.selector).dispatchEvent(new Event("change", {
                    view: window,
                    bubbles: true,
                    cancelable: true
                }));
                doLoop(i);
                break;
            case "click" :
                document.querySelector(el.selector).dispatchEvent(new MouseEvent("click", {
                    view: window,
                    bubbles: true,
                    cancelable: true
                }));
                doLoop(i);
                break;
            case "event" :
                document.querySelector(el.selector).dispatchEvent(new Event(el.value));
                doLoop(i);
                break;
			case "today" :
				var d = new Date();
				var month = d.getMonth()+1;
				var month = month.toString().length < 2 ? "0"+month : month;
				var today = d.getDate() + "/" + month + "/" + d.getUTCFullYear();
                document.querySelector(el.selector).value = today.toString();
                doLoop(i);
                break;
            case "wait" :
                setTimeout(function() {doLoop(i);},el.value);

        }
    } catch (e) {
        console.error("Failed to do action, skipping.", el);
        doLoop(i);
    }
}
