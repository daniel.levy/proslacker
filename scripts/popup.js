var list, newNode, refNode;
document.addEventListener('DOMContentLoaded', function () {
    var tabId;

    document.querySelector("button#reset").addEventListener("click", resetData);

    document.querySelector("button#spit").addEventListener("click", spitList);

    document.addEventListener("dragover", function(event) {
        // prevent default to allow drop
        event.preventDefault();
    }, false);

    loadList();
});

function spitList() {
	console.log(JSON.stringify(list));
}

function deleteRow(element) {
    element.parentElement.parentElement.remove();
    saveData();
}

function appendItemRow(before) {
    var table = document.querySelector("#list");
    var row = makeItemRow();
    table.insertBefore(row,before);
    saveData();
}

function appendPageRow(before) {
    var table = document.querySelector("#list");
    var row = makePageRow();
    table.insertBefore(row,before);
    saveData();
}

function saveData() {
    var data = [];
    var rows = document.querySelector("#list").children;
    for(var i = 0, pi = -1 , ii=0; i < rows.length; i++) {
        if(rows[i].className.includes("page") || (i == 0 && rows[i].className.includes("item"))) {
            pi++;
            data.push({});
            var page = rows[i].querySelector("input.page");
            data[pi].page = page != null ? page.value : "";
            data[pi].items = [];

            if(rows[i].getAttribute("data-hidden") == "true") {
                data[pi].hidden = true;
            } else {
                data[pi].hidden = false;
            }

            ii = 0;
        }
        if(rows[i].className.includes("item")) {
            if(data.length == 0) {
                data.push({});
                data[pi].items = [];
            }
            data[pi].items.push({});
            data[pi].items[ii].selector = rows[i].querySelector("input.selector").value;
            data[pi].items[ii].type = rows[i].querySelector("select.type").value;
            data[pi].items[ii].value = rows[i].querySelector("input.value").value;
            ii++;
        }
    }
    chrome.storage.local.set({list: data}, function() {
      console.info('Set storage.');
      loadList();
    });
}

function resetData() {
    if(confirm("Are you sure?")) {
        chrome.storage.local.set({list: null}, function() {
          console.info('Reset storage.');
          loadList();
        });
    }
}

function loadList() {
    chrome.storage.local.get(['list'], function(result) {
        console.info("Attempting to initialise storage.");
        if(result.list == null) {
            chrome.storage.local.set({list: listOfPages}, function() {
              console.info('Initialised storage.');
              list = listOfPages;
            });
        } else {
            console.info('Storage already set.');
            list = result.list;
        }
        makeTable();
    });
}

function makeTable() {
    var table = document.querySelector("#list");
    table.innerHTML = "";
    for(var i = 0; i < list.length; i++) {
        table.appendChild(makePageRow(list[i].page, list[i].hidden));
        for(j = 0; j < list[i].items.length; j++) {
            table.appendChild(makeItemRow(list[i].items[j], list[i].page, list[i].hidden));
        }
        table.appendChild(makeButtonRow(list[i].page, list[i].hidden));
    }

    table.addEventListener("drop", function(e){
        e.preventDefault();
        table.insertBefore(newNode, refNode);
        saveData();
    });

    return table;
}

function makeItemRow(listItem = {}, page = "", hidden = false){
    var row = document.createElement("DIV");
    var cells = '<div class="col-1" draggable="true"><img src="../images/reorder.png" /></div>';
    cells += '<div class="col-3"> <input ' + (listItem.type == "wait" ? "disabled" : "") + ' placeholder="selector" class="selector" value="' + (listItem.selector || '')  + '"/> </div>';
    cells += '<div class="col-2">' + makeSelect(listItem.type) + '</div>';
    cells += '<div class="col-3"> <input ' + (listItem.type == "click" || listItem.type == "today" ? "disabled" : "") + ' placeholder="value" class="value" value="' + (listItem.value || '') + '"/> </div>';
    cells += '<div class="col-1"><button class="delete">-</button></div>';
    row.className = 'row item ' + page + '-row' +  (hidden ? " hidden" : "");
    row.innerHTML = cells;

    var fields = row.querySelectorAll("input,select");
    for(var i = 0; i<fields.length; i++) {
        fields[i].addEventListener("change",function(e) {
            saveData();
        });
    }

    row.querySelector("button.delete").addEventListener("click", function(e) {
        if(confirm("Are you sure?")) {
            deleteRow(e.srcElement);
        }
    });

    row.addEventListener("dragstart", function(){
        newNode = this;
        addClass(this,"dragged");
    });

    row.addEventListener("dragover", function(){
        refNode = this;
        addClass(this,"bordered");
    });

    row.addEventListener("dragleave", function(){
        removeClass(this,"bordered");
    });


    return row;
}

function makePageRow(page = "", hidden = false){
    var row = document.createElement("DIV");
    var cells = '<div><button class="run" data-target="'+page+'">Run</button></div>';
    cells += '<div class="col-1" draggable="true"><img src="../images/reorder.png" /></div>';
    cells += '<div class="col-8"><input placeholder="page" class="page" value="' + page + '"/></div>';
    cells += '<div class="col-1"><button class="delete">-</button></div>';
    cells += '<div class="col-2"><button data-target="'+page+'" class="hide">Hide</button>';
    cells += '<button data-target="'+page+'" class="show">Show</button></div>';

    row.classList.add("row","page");

    row.innerHTML = cells;
    row.setAttribute("data-hidden", hidden);

    if(hidden) {
        addClass(row.querySelector("button.hide"),"hidden");
    } else {
        addClass(row.querySelector("button.show"),"hidden")
    }

    row.querySelector("input").addEventListener("change",function(e) {
        saveData();
    });

    row.querySelector("button.delete").addEventListener("click", function(e) {
        if(confirm("Are you sure?")) {
            deleteRow(e.srcElement);
        }
    });

    row.querySelector("button.hide").addEventListener("click", function(e){
        hideRows(e.srcElement);
        addClass(this,"hidden");
        removeClass(row.querySelector("button.show"),"hidden");
    });

    row.querySelector("button.show").addEventListener("click", function(e){
        showRows(e.srcElement);
        addClass(this,"hidden");
        removeClass(row.querySelector("button.hide"),"hidden");
    });

    row.addEventListener("dragstart", function(){
        addClass(this,"dragged");
        newNode = this;
    });

    row.addEventListener("dragover", function(){
        refNode = this;
        addClass(this,"bordered");
    });

    row.addEventListener("dragleave", function(){
        removeClass(this,"bordered");
    });

    row.querySelector("button.run").addEventListener("click", function(e) {
        var currentPage = e.target.getAttribute("data-target");
        chrome.storage.local.set({currentPage: currentPage});
        chrome.tabs.query({active:true},function(tabs) {
            chrome.scripting.executeScript({
                files:["scripts/do.js"],
                target: {tabId: tabs[0].id}
            });
        });
    });

    return row;
}

function makeButtonRow(page = "", hidden = false){
    var row = document.createElement("DIV");
    var cells = '<div class="col-6">';
    cells += '<button class="add-item">Add Item</button>';
    cells += '<button class="add-page">Add Page</button>';
    cells += '</div>';
    row.innerHTML = cells;

    row.classList.add("row",page+"-row");
    toggleClass(row,"hidden", hidden)

    row.querySelector("button.add-item").addEventListener("click", function() {
        appendItemRow(row);
    });
    row.querySelector("button.add-page").addEventListener("click", function() {
        appendPageRow(row);
    });

    row.addEventListener("dragover", function(){
        refNode = this;
        addClass(this,"bordered");
    });

    row.addEventListener("dragleave", function(){
        removeClass(this,"bordered");
    });

    return row;
}

function makeSelect(type) {
    var types = ["click","event","select","text","wait","today"];
    var select = "<select class='type'>";

    for (var i = 0; i < types.length; i++){
        select += "<option value='"+ types[i] +"' "+ (type==types[i]
            ? 'selected' : '') +">"+types[i]+"</option>";
    }

    select += "</select>";

    return select;
}

function hideRows(element) {
    var page = element.getAttribute("data-target");
    element.parentElement.parentElement.setAttribute("data-hidden", true);
    var rows = document.querySelectorAll("."+page+"-row");
    for(var i = 0; i < rows.length; i++) {
        addClass(rows[i],"hidden");
    }
    saveData();
}

function showRows(element) {
    var page = element.getAttribute("data-target");
    element.parentElement.parentElement.setAttribute("data-hidden", false);
    var rows = document.querySelectorAll("."+page+"-row");
    for(var i = 0; i < rows.length; i++) {
        removeClass(rows[i],"hidden");
    }
    saveData();
}

function addClass(element, theclass) {
    element.classList.toggle(theclass, true);
}

function removeClass(element, theclass) {
    element.classList.toggle(theclass, false);
}

function toggleClass(element, theclass, state) {
    element.classList.toggle(theclass, state);
}
